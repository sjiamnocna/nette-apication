<?php

// absolute namespace needed due to composer namespace
use \APIcation\Bootstrap;
use \APIcation\CLI\CRunner;

if ($argc < 2){
    // no arguments
    throw new Exception(sprintf('This command needs at least 2 values. %i given. %s', $argc, var_export($argv, true)));
}

// include autoloader
include $_composer_autoload_path;
// in case it don't work, check this path and also Namespace to your Bootstrap class
// as defauld I suppose your path is /app/bootstrap.php
require_once $_composer_bin_dir . '/../../app/Bootstrap.php';

// create the same environment as with whole app
Bootstrap::boot()
  ->createInstance(CRunner::class)
  ->run( $argv );