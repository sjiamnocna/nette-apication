<?php declare(strict_types = 1);

namespace APIcation;

use Nette\SmartObject;

class CResponse
{
    use SmartObject;

    /**
     * Set properties in constructor
     *
     * @param array        $data   Data to send to the client
     * @param array|string $action Action(s) to perform on the client
     * @param int          $code   Return code
     */
    public function __construct(
      private array         $data,
      private string|array  $action = [],
      private int           $code = 200
    ){
    }

    /**
     * @return string|null
     */
    public function getAction(): string|array
    {
        return $this->action;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * Return data in JSON format
     *
     * @param array $overwrite Allow last call overwrite code/data/actions via assoc array
     *
     * @return string
     */
    private function getJson(array $overwrite = []): string
    {
        $data = [
          'code' => $this->code,
          'data' => $this->data,
        ];

        if (!empty($this->action)){
            // add action only if was specified, convert it to array
            $data['action'] = is_array($this->action) ? $this->action : [$this->action];
        }

        if ($overwrite){
            // overwrite by merging data in each assoc key
            $data = [
              'code' => $overwrite['code'] ?? $data['code'],
              'action' => isset($overwrite['action']) ? array_merge($data['action'], $overwrite['action']) : $data['action'],
              'data' => isset($overwrite['data']) ? array_merge($data['data'], $overwrite['data']) : $data['data'],
            ];
        }

        return json_encode($data);
    }

    /**
     * Alias for getJson method
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->getJson();
    }

    /**
     * Send the response - set headers and print JSON
     *
     * @param array $overwrite  Array of last-call overwriting values
     * @param int   $httpCode   HTTP code to send with the response
     *
     * @return void
     */
    public function send(array $overwrite = [], int $httpCode = 200): void
    {
        if ($httpCode !== 200){
            // if specified code is set, add it to the HTTP response
            http_response_code($httpCode);
        }

        header('Content-Type: application/json; charset=utf-8');
        die( $this->getJson($overwrite) );
    }
}