<?php declare(strict_types = 1);

namespace APIcation\CLI;

interface IRunnerCommand
{
    /**
     * Runs action for given command class
     *
     * @param array $args Arguments passed in CMD command
     */
    function run( array $args ): void;
}